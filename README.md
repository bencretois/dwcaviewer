# DwCaViewer

### What is Darwin Core Archive?

Darwin Core Archive (DwC-A) is a biodiversity informatics data standard that makes use of the Darwin Core terms to produce a single, self-contained dataset for species occurrence, checklist, sampling event or material sample data. Essentially it is a set of text (CSV) files with a simple descriptor (meta.xml) to inform others how your files are organized. The format is defined in the Darwin Core Text Guidelines. It is the preferred format for publishing data to the GBIF network. For more information see [here](https://dwc.tdwg.org/text/)

### Why using this application?

The DwCaViewer provides a simple way to quickly visualize what the dataset you are interested in is composed of. Numerous summary statistics are available including ggplot graphs and a map of observations. This application also allows to have an overview of the dataset's metadata.

### How to use this application?

You can use this application in two different ways:

- The application is hosted on a server and can be accessed at: XXX
- Use the function `dwcviewer()` from the `LivingNorwayR` package

### Author

<img src="man/figures/logo_nina.png" alt="drawing" width="100"/> <img src="man/figures/ln.png" alt="drawing" width="75"/>

DwCaViewer was created at the [Norwegian Institute for Nature Research](https://www.nina.no/english/home) by [Benjamin Cretois](https://benjamincretois.netlify.app/) and was funded by [Living Norway](https://livingnorway.no/), a project aiming at promoting the management of ecological data from Norwegian research institutions in agreement with [FAIR](https://www.nature.com/articles/sdata201618) data principles.

### Copyright

[![License](https://img.shields.io/badge/Licence-GPL%20v2.0-orange.svg)]() DwCaViewer is licensed under the GNU General Public License (GPL) v2.0. 




