## app.R ##
source("/home/rstudio/app/appScripts/dependancies.R")
source("/home/rstudio/app/appScripts/global.R")
source("/home/rstudio/app/appScripts/custom_theme.R")
source("/home/rstudio/app/appScripts/ui.R")
source("/home/rstudio/app/appScripts/server.R")

options(shiny.maxRequestSize=30*1024^2)
options(warn = -1)

app <- shinyApp(ui = ui, server = server)
runApp(app, host ="0.0.0.0", port = 8999, launch.browser = FALSE)

# Can test the dataset with:
# 6a948a1c-7e23-4d99-b1c1-ec578d0d3159 ptarmigan survey
# 0ae8db45-88e0-452c-986b-c7e62393efe4 water beetles

# b848f1f3-3955-4725-8ad8-e711e4a9e0ac

# TOV-E dataset : 4a00502d-6342-4294-aad1-9727e5c24041

# SOME OCCURRENCE DATASET

# 6f09416a-7540-420a-ab66-d3223ff3af48 // Pukkellaks i Norge 2017
